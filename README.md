
# masterdb-data

  

## GetItemsByAsins.json

  

### historical_data

***amazon***

*0 - amazon_landed_price*

*1 - amazon_listing_price*

*2 - amazon_shipping*

*3 - timestamp*

  

***amazon_warehouse***

*0 - amazon_warehouse_landed_price*

*1 - amazon_warehouse_listing_price*

*2 - amazon_warehouse_shipping*

*3 - timestamp*

  

***buybox_new***

*0 - buy_box_sellerid_new*

*1 - buy_box_landed_price_new*

*2 - buy_box_listing_price_new*

*3 - buy_box_shipping_new*

*4 - buy_box_type_new*

*5 - total_offers_new*

*6 - timestamp*

  

***buybox_used***

*0 - buy_box_sellerid_used*

*1 - buy_box_landed_price_used*

*2 - buy_box_listing_price_used*

*3 - buy_box_shipping_used*

*4 - buy_box_type_used*

*5 - total_offers_used*

*6 - timestamp*

  

***lowest_fba_new***

*0 - lowest_landed_price_new_fba*

*1 - lowest_listing_price_new_fba*

*2 - lowest_shipping_new_fba*

*3 - timestamp*

  

***lowest_fba_used***

*0 - lowest_landed_price_used_fba*

*1 - lowest_listing_price_used_fba*

*2 - lowest_shipping_used_fba*

*3 - timestamp*

  

***lowest_fba_used_acceptable***

*0 - lowest_landed_price_used_acceptable_fba*

*1 - lowest_listing_price_used_acceptable_fba*

*2 - lowest_shipping_used_acceptable_fba*

*3 - timestamp*

  

***lowest_fba_used_good***

*0 - lowest_landed_price_used_good_fba*

*1 - lowest_listing_price_used_good_fba*

*2 - lowest_shipping_used_good_fba*

*3 - timestamp*

  

***lowest_fba_used_like_new***

*0 - lowest_landed_price_used_like_new_fba*

*1 - lowest_listing_price_used_like_new_fba*

*2 - lowest_shipping_used_like_new_fba*

*3 - timestamp*

  

***lowest_fba_used_very_good***

*0 - lowest_landed_price_used_very_good_fba*

*1 - lowest_listing_price_used_very_good_fba*

*2 - lowest_shipping_used_very_good_fba*

*3 - timestamp*

  

***lowest_fbm_new***

*0 - lowest_landed_price_new_fbm*

*1 - lowest_listing_price_new_fbm*

*2 - lowest_shipping_new_fbm*

*3 - timestamp*

  

***lowest_fbm_used***

*0 - lowest_landed_price_used_fbm*

*1 - lowest_listing_price_used_fbm*

*2 - lowest_shipping_used_fbm*

*3 - timestamp*

  

***lowest_fbm_used_acceptable***

*0 - lowest_landed_price_used_acceptable_fbm*

*1 - lowest_listing_price_used_acceptable_fbm*

*2 - lowest_shipping_used_acceptable_fbm*

*3 - timestamp*

  

***lowest_fbm_used_good***

*0 - lowest_landed_price_used_good_fbm*

*1 - lowest_listing_price_used_good_fbm*

*2 - lowest_shipping_used_good_fbm*

*3 - timestamp*

  

***lowest_fbm_used_like_new***

*0 - lowest_landed_price_used_like_new_fbm*

*1 - lowest_listing_price_used_like_new_fbm*

*2 - lowest_shipping_used_like_new_fbm*

*3 - timestamp*

  

***lowest_fbm_used_very_good***

*0 - lowest_landed_price_used_very_good_fbm*

*1 - lowest_listing_price_used_very_good_fbm*

*2 - lowest_shipping_used_very_good_fbm*

*3 - timestamp*

  

***offers_new***

*0 - total_offers_new*

*1 - total_offers_new_fba*

*2 - total_offers_new_fbm*

*3 - timestamp*

  

***offers_used***

*0 - total_offers_used*

*1 - total_offers_used_fba*

*2 - total_offers_used_fbm*

*3 - timestamp*

  

***review_rating***

*0 - rating*

*1 - rating_count*

*2 - timestamp*

  

***root_rank***

*0 - root_category*

*1 - rank_of_root_category*

*2 - timestamp*

  

***sub_rank***

*0 - sub_category_id*

*1 - rank_of_sub_category*

*2 - timestamp*

  

***sales***

*0 - est_sales*

*1 - timestamp*

  

***suppressed_status***

*0 - timestamp*

  

***unavailable_status***

*0 - timestamp*

  

## ProductBuyboxStats

  

### buybox_history

  

*0 - landed_price*

*1 - listing_price*

*2 - shipping_price*

*3 - buy_box_type*

*4 - total_offer*

*5 - timestamp*

  

## SellerMetrics

  

### history

*0 - feedback_rating*

*1 - feedback_count*

*2 - timestamp*
